/*
    SPDX-FileCopyrightText: 2020 Marco Martin <mart@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick 2.12
import QtQuick.Window 2.12
import org.kde.kirigami 2.14 as Kirigami

Kirigami.AbstractApplicationWindow {
    title: navigator.layers.depth > 1
           ? navigator.layers.currentItem.title
           : navigator.pages[navigator.currentIndex].title
    pageStack: navigator
    Kirigami.SwipeNavigator {
        id: navigator
        anchors.fill: parent
        pages: [
            PublicFeedPage{
                title: i18n("Public Feed")
                url: 'https://mastodon.technology/api/v1/timelines/public'
            },
            PublicFeedPage{
                 title: i18n("Fosdem")
                 url: 'https://mastodon.technology/api/v1/timelines/tag/fosdem'
            },
            PublicFeedPage{
                 title: i18n("KDE")
                 url: 'https://mastodon.technology/api/v1/timelines/tag/kde'
            }
        ]
    }
}
