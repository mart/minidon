/*
    SPDX-FileCopyrightText: 2020 Marco Martin <mart@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick 2.14
import QtQuick.Layouts 1.4
import QtQuick.Controls 2.4
import org.kde.kirigami 2.14 as Kirigami

Kirigami.ScrollablePage {
    id: root

    icon.name: "view-pim-news"
    property string url: 'https://mastodon.technology/api/v1/timelines/public'
    supportsRefreshing: true
    onRefreshingChanged: {
        if (refreshing) {
            refresh()
        }
    }

    header: ToolBar {
        visible: root.StackView.view || Kirigami.Settings.isMobile
        height: visible ? implicitHeight : 0
        contentItem: RowLayout {
            ToolButton {
                visible: root.StackView.view
                icon.name: "go-previous"
                text: i18n("Back")
                onClicked: root.StackView.view.pop()
            }
            Kirigami.Heading {
                Layout.fillWidth: true
                text: root.title
            }
        }
    }
    // this function is included locally, but you can also include separately via a header definition
    function request(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = (function(myxhr) {
            return function() {
                callback(myxhr);
            }
        })(xhr);
        xhr.open('GET', url, true);
        xhr.send('');
    }

    function refresh() {
        request(root.url, function (o) {

            // log the json response
            console.log(o.responseText);

            // translate response into object
            //var d = eval('new Object(' + o.responseText + ')');
            var d = JSON.parse(o.responseText);

            // access elements inside json object with dot notation
            view.model = d;
            root.refreshing = false;
        });
    }

    Component.onCompleted: root.refreshing = true;

    Kirigami.CardsListView {
        id: view
        delegate: Kirigami.Card {
            contentItem: GridLayout {
                columns: 2
                Kirigami.Avatar {
                    Layout.alignment: Qt.AlignTop
                    Layout.rowSpan: 2
                    source: modelData.account.avatar
                    name: modelData.account.display_name
                    TapHandler {
                        onTapped: navigator.layers.push("PublicFeedPage.qml", {title: modelData.account.display_name, url:"https://mastodon.technology/api/v1/accounts/"+modelData.account.id+"/statuses"})
                    }
                }
                RowLayout {
                    Layout.fillWidth: true
                    Label {
                        Layout.fillWidth: true
                        text: i18n("%1 @%2 -", modelData.account.display_name, modelData.account.username)
                        elide:Text.ElideRight
                    }
                    Label {
                        text: Date(modelData.created_at).toString()
                    }
                }
                Label {
                    Layout.fillWidth: true
                    text: modelData.content
                    wrapMode: Text.Wrap
                }
            }
        }
    }
}
